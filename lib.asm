

%define SYS_WRITE 1
%define SYS_EXIT 60
%define BASE10 0xA
%define stderr 2
%define stdout 1
%define NEWLINE 0xA
%define SPACE 0x20
%define TAB 0x9

section .text

global string_length
global print_char
global print_newline
global print_string
global print_error
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy
global exit
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
    .loop:
        cmp byte [rdi+rcx], 0
        je .exit
    	inc rcx
    	jmp .loop
    .exit:
        mov rax, rcx
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

    push rdi
    call string_length
    pop rsi

    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1    
    
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    call print_char
    ret

; Выводит строку в stderr
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax 
    mov rax, SYS_WRITE
    mov rdi, stderr
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    mov rax, rdi
    mov r10, BASE10 ; the base we are working with
    xor r9, r9 ; digit count
    .loop:
        xor rdx, rdx
        div r10
        inc r9 ; new digit
        push rdx; pushing the remainder of rax/10 = last digit
        cmp rax, 0
        je .prnt ; rax != 0 means we haven't finished yet
        jmp .loop
    .prnt:
        pop rdi ; retrieving the last digit
        add rdi, 0x30 ; converting into ASCII

        push rcx
        push r9
        push r10 ; storing caller-saved registers by convention
        call print_char
        pop r10
        pop r9
        pop rcx

        dec r9
        jnz .prnt ; jump if remaining digit count still not zero
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .prnt
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

    .prnt:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
	mov dl, byte[rdi+rax]
	cmp byte[rsi + rax], dl
	jne .err
	inc rax
	cmp dl, 0
	jne .loop
	mov rax, 1
	ret
    .err:
	xor rax, rax
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
%define SIZEOF_CHAR 1
read_char:
    xor rax, rax
    push 0
    mov rsi, rsp 
    xor rdi, rdi ;stdin
    mov rdx, SIZEOF_CHAR
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор



read_word:
    xor rax, rax
    xor rcx, rcx
    push rdi
    mov r10, rdi
    mov r9, rsi

    .loop:
        push rcx
        push r10
        push r9
        call read_char
        pop r9
        pop r10
        pop rcx

        cmp rax, NEWLINE
        je .skip
        cmp rax, SPACE
        je .skip
        cmp rax, TAB
        je .skip
        cmp rax, 0
        je .end

        cmp rcx, r9
        jge .overflow

        mov byte[r10 + rcx], al
        inc rcx
        jmp .loop
    .skip:
        cmp rcx, 0
        je .loop ; if not in the beginning, goes to .end (aka the word has ended)

    .end:
        mov byte[r10 + rcx], 0
        pop rax
        mov rdx, rcx
        ret
    .overflow:
        pop rax
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    mov r10, 10

    .loop:
        push rsi ; as a counter
        mov sil, [rdi]; as a digit
        
        test sil, sil
        je .end ; null char
        cmp sil, 0xA ; newline
        je .end
        
        cmp sil, '0'
        jb .end ; if char < '0'
        cmp sil, '9'
        ja .end ; if char > '9'

        ; adding a 'digit' to rax
        sub sil, 0x30
        mul r10
        add rax, rsi

        inc rdi
        pop rsi
        inc rsi

        jmp .loop
    .end:
        pop rdx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .neg
    .pos:
        call parse_uint
        ret
    .neg:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .i
        inc rdx ; increase len by 1 (unless err val) to make up for the minus sign
        .i:
            neg rax
            ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:

    push rdi ;str pointer
    push rsi ;buf pointer
    push rdx ;buf len
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rdx, rax
    jl .overflow

    xor rcx, rcx
    xor rdx, rdx
    .loop:
        mov cl, [rdi + rdx]
        mov [rsi + rdx], cl
        inc rdx
        cmp rdx, rax
        jle .loop
        ret
    .overflow:
        xor rax, rax
        ret
