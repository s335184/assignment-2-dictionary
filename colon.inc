%define next_element 0
%macro colon 2
%2:
    dq next_element
    db %1, 0
%define next_element %2
%endmacro

