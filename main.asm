%include "lib.inc"
extern find_word

%define BUF_SIZE 255
%define ERROR_CODE 1

section .bss
buffer: resb BUF_SIZE 

section .rodata
error_buffer_size: db "Buffer size exceeded! Only 255 bytes are available.", 10, 0
error_not_found: db "No such key in the dictionary.", 10, 0
%include "words.inc"

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, BUF_SIZE
	call read_word
	test rax, rax
	je .buffer_exceeded

	mov rsi, next_element
	mov rdi, buffer
	call find_word
	test rax, rax
	je .key_not_found

	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit

.key_not_found:
	mov rdi, error_not_found
	call print_error
	mov rdi, ERROR_CODE
	call exit
.buffer_exceeded:
	mov rdi, error_buffer_size
	call print_error
	mov rdi, ERROR_CODE
	call exit

