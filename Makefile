AFLAGS=-felf64
ASM=nasm

all: main

main: lib.o main.o dict.o 
	ld -o $@ $?

main.o: main.asm colon.inc words.inc lib.inc
	$(ASM) $(AFLAGS) main.asm

lib.o: lib.asm 
	$(ASM) $(AFLAGS) lib.asm

dict.o: dict.asm 
	$(ASM) $(AFLAGS) dict.asm


clean:
	rm -f main.o lib.o dict.o main
