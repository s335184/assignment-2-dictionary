global find_word
extern string_equals

section .text

; accepts the address of the name string and address of the last word (in rdi and rsi respectively)
; returns address in rax if found, otherwise returns 0
find_word: 
    xor rax, rax
.loop:
    test rsi, rsi 
    jz .end 
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    jmp .loop
.found:
    mov rax, rsi
.end:
    ret
